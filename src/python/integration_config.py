#
# Imports
#

# stdlib
import json
import logging

# certifi
import certifi

# urllib3
import urllib3


# sentry_sdk
from sentry_sdk.integrations import Integration


#
# Constants
#

CONFIG_URL = 'https://sentry-config.openwg.net/v1/python.json'



#
# Integration
#

class OpenWGConfigIntegration(Integration):
    identifier = "openwg_config"

    def __init__(self):
        self.logger = logging.getLogger('OpenWG/BugReports/Config')
        self.enabled = True
        self.ignore_list = {
            'log' : [
                'Failed to attach VehicleStickers. Missing VehicleStickers.'
            ],
            'exc' : []
        }

        data, errmsg = self.get_online_config()
        if not data:
            self.logger.error('get config: %s' % errmsg)
            return

        config = None
        try:
            config = json.loads(data)
        except Exception as ex:
            self.logger.error('parse config: %s' % str(ex))
            return

        try:
            self.enabled = config.get('enabled', self.enabled)
            self.ignore_list = config.get('ignore_list', self.ignore_list)   
        except Exception as ex:
            self.logger.exception('apply config')
            return

    def get_online_config(self):
        http = urllib3.PoolManager(
            cert_reqs="CERT_REQUIRED",
            ca_certs=certifi.where()
        )

        # Make the request using the headers
        response = None
        response_errmsg = ''
        try:
            headers = {
                "Accept": "application/json",
                "Accept-Encoding": "gzip",
            }
            timeout = urllib3.Timeout(connect=2.0, read=2.0)
            retries = urllib3.Retry(total=2, redirect=1)
            response = http.request("GET", CONFIG_URL, headers=headers, timeout=timeout, retries=retries)
        except urllib3.exceptions.TimeoutError:
            response_errmsg = 'timeout'
        except urllib3.exceptions.MaxRetryError:
            response_errmsg = 'maximum retries count'
        except Exception as ex:
            response_errmsg = str(ex)

        if response and response.status in [200]:
            return (response.data, response_errmsg)
        else:
            return (None, response_errmsg)


    # Sentry API

    @staticmethod
    def setup_once():
        pass

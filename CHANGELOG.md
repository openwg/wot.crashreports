# Changelog

## v10.2.0

* gather mod version from `__version__`

## v10.1.9

* add modpack detection

## v10.1.8

* fix modules data

## v10.1.7

* fix initialization

## v10.1.6

* change DSNs

## v10.1.5

* switch to OpenWG VFS

## v10.1.4

* serverside filter for exceptions

## v10.1.3

* add serverside filter update

## v10.1.2

* filter out WG error to reduce pressure on server

## v10.1.1

* turn off debug

## v10.1.0

* extended information

## v10.0.0

* first release as a separate project